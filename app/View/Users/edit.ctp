<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('login');
		echo $this->Form->input('password');
		echo $this->Form->input('nom');
		echo $this->Form->input('prenom');
		echo $this->Form->input('role_id');
		echo $this->Form->input('email');
		echo $this->Form->input('email_quanteam');
		echo $this->Form->input('image');
		echo $this->Form->input('tel');
		echo $this->Form->input('ville');
		echo $this->Form->input('adresse');
		echo $this->Form->input('natio');
		echo $this->Form->input('category_id');
		echo $this->Form->input('default_lang');
		echo $this->Form->input('date_creation');
		echo $this->Form->input('active');
		echo $this->Form->input('reset_password_token');
		echo $this->Form->input('token_created_at');
		echo $this->Form->input('change_stat');
		echo $this->Form->input('permis_tra');
		echo $this->Form->input('sexe');
		echo $this->Form->input('code_post');
		echo $this->Form->input('ne_le');
		echo $this->Form->input('to_a');
		echo $this->Form->input('jeune_fille');
		echo $this->Form->input('etat_civil');
		echo $this->Form->input('etat_pays_nais');
		echo $this->Form->input('entite_interne_id');
		echo $this->Form->input('last_role');
		echo $this->Form->input('tork_mdp');
		echo $this->Form->input('comp_adresse');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
	</ul>
</div>
