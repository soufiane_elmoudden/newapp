<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Login'); ?></dt>
		<dd>
			<?php echo h($user['User']['login']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($user['User']['nom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prenom'); ?></dt>
		<dd>
			<?php echo h($user['User']['prenom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['role_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email Quanteam'); ?></dt>
		<dd>
			<?php echo h($user['User']['email_quanteam']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($user['User']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tel'); ?></dt>
		<dd>
			<?php echo h($user['User']['tel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ville'); ?></dt>
		<dd>
			<?php echo h($user['User']['ville']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adresse'); ?></dt>
		<dd>
			<?php echo h($user['User']['adresse']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Natio'); ?></dt>
		<dd>
			<?php echo h($user['User']['natio']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['category_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Default Lang'); ?></dt>
		<dd>
			<?php echo h($user['User']['default_lang']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Creation'); ?></dt>
		<dd>
			<?php echo h($user['User']['date_creation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($user['User']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reset Password Token'); ?></dt>
		<dd>
			<?php echo h($user['User']['reset_password_token']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Token Created At'); ?></dt>
		<dd>
			<?php echo h($user['User']['token_created_at']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Change Stat'); ?></dt>
		<dd>
			<?php echo h($user['User']['change_stat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Permis Tra'); ?></dt>
		<dd>
			<?php echo h($user['User']['permis_tra']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sexe'); ?></dt>
		<dd>
			<?php echo h($user['User']['sexe']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Post'); ?></dt>
		<dd>
			<?php echo h($user['User']['code_post']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ne Le'); ?></dt>
		<dd>
			<?php echo h($user['User']['ne_le']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('To A'); ?></dt>
		<dd>
			<?php echo h($user['User']['to_a']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jeune Fille'); ?></dt>
		<dd>
			<?php echo h($user['User']['jeune_fille']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Etat Civil'); ?></dt>
		<dd>
			<?php echo h($user['User']['etat_civil']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Etat Pays Nais'); ?></dt>
		<dd>
			<?php echo h($user['User']['etat_pays_nais']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Entite Interne Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['entite_interne_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Role'); ?></dt>
		<dd>
			<?php echo h($user['User']['last_role']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tork Mdp'); ?></dt>
		<dd>
			<?php echo h($user['User']['tork_mdp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comp Adresse'); ?></dt>
		<dd>
			<?php echo h($user['User']['comp_adresse']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
	</ul>
</div>
