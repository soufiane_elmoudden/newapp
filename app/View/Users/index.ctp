<div class="users index">
	<h2><?php echo __('Users'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('login'); ?></th>
			<th><?php echo $this->Paginator->sort('password'); ?></th>
			<th><?php echo $this->Paginator->sort('nom'); ?></th>
			<th><?php echo $this->Paginator->sort('prenom'); ?></th>
			<th><?php echo $this->Paginator->sort('role_id'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('email_quanteam'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('tel'); ?></th>
			<th><?php echo $this->Paginator->sort('ville'); ?></th>
			<th><?php echo $this->Paginator->sort('adresse'); ?></th>
			<th><?php echo $this->Paginator->sort('natio'); ?></th>
			<th><?php echo $this->Paginator->sort('category_id'); ?></th>
			<th><?php echo $this->Paginator->sort('default_lang'); ?></th>
			<th><?php echo $this->Paginator->sort('date_creation'); ?></th>
			<th><?php echo $this->Paginator->sort('active'); ?></th>
			<th><?php echo $this->Paginator->sort('reset_password_token'); ?></th>
			<th><?php echo $this->Paginator->sort('token_created_at'); ?></th>
			<th><?php echo $this->Paginator->sort('change_stat'); ?></th>
			<th><?php echo $this->Paginator->sort('permis_tra'); ?></th>
			<th><?php echo $this->Paginator->sort('sexe'); ?></th>
			<th><?php echo $this->Paginator->sort('code_post'); ?></th>
			<th><?php echo $this->Paginator->sort('ne_le'); ?></th>
			<th><?php echo $this->Paginator->sort('to_a'); ?></th>
			<th><?php echo $this->Paginator->sort('jeune_fille'); ?></th>
			<th><?php echo $this->Paginator->sort('etat_civil'); ?></th>
			<th><?php echo $this->Paginator->sort('etat_pays_nais'); ?></th>
			<th><?php echo $this->Paginator->sort('entite_interne_id'); ?></th>
			<th><?php echo $this->Paginator->sort('last_role'); ?></th>
			<th><?php echo $this->Paginator->sort('tork_mdp'); ?></th>
			<th><?php echo $this->Paginator->sort('comp_adresse'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['login']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['password']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['nom']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['prenom']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['role_id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email_quanteam']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['image']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['tel']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['ville']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['adresse']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['natio']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['category_id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['default_lang']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['date_creation']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['active']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['reset_password_token']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['token_created_at']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['change_stat']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['permis_tra']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['sexe']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['code_post']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['ne_le']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['to_a']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['jeune_fille']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['etat_civil']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['etat_pays_nais']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['entite_interne_id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['last_role']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['tork_mdp']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['comp_adresse']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?></li>
	</ul>
</div>
